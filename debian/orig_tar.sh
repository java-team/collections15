#!/bin/bash

VERSION="$2"
FILENAME="$3"

TEMPDIR=`mktemp -d`
NEWFILENAME="`echo "$FILENAME" | sed 's|.orig.tar.gz$||'`+ds1.orig.tar.gz"

echo "$NEWFILENAME"

tar xzf "$FILENAME" -C "$TEMPDIR"
MAINDIR="`ls "$TEMPDIR"`"
find "$TEMPDIR" -iname '*.jar' -print0 | xargs -0 rm -vf
tar czf "$NEWFILENAME" -C "$TEMPDIR" "$MAINDIR"

rm -fr "$TEMPDIR"
